#include<stdio.h>
#include<math.h>
int main()
{
    int discriminant,a,b,c,root1,root2;
    printf("Enter the cooefficient of x^2:\n");
    scanf("%d",&a);
    printf("Enter the cooefficient of x:\n");
    scanf("%d",&b);
    printf("Enter the value of constant:\n");
    scanf("%d",&c);
    discriminant=b*b-4*a*c;
    
    if(discriminant>0)
    {
        root1=(-b+sqrt(discriminant))/(2*a);
        root2=(-b-sqrt(discriminant))/(2*a);
        printf("The roots are: %d & %d\n",root1,root2);
    }
    
    else if(discriminant==0)
    {
        int root=-b/(2*a);
        printf("The root is %d",root);
    }
    
    else
    {
        printf("The roots are imaginary");
    }
    
    return 0;
}
    
