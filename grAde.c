#include <stdio.h>
int input()
{
    int x;
    printf("Enter marks\n");
    scanf("%d",&x);
    return x;
}
int check(int a)
{
    int val=10;
    if(a>=90)
        val=0;
    if((a>70)&&(a<90))
        val=1;
    if((a>60)&&(a<70))
        val=2;
    if((a>40)&&(a<60))
        val=3;
    return val;
}
void output(int a)
{
    switch(a)
    {
        case 0:
            printf("Grade A\n");
            break;
        case 1:
            printf("Grade B\n");
            break;
        case 2:
            printf("Grade C\n");
            break;
        case 3:
            printf("Grade D\n");
            break;
        default:
            printf("Fail\n");
            break;
        
    }
}
int main()
{
    int a,val;
    a=input();
    val =check(a);
    output(val);
    return 0;
}
